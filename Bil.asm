; ***********************************************
; 		AVR Pole Position Main program		    * 
; 		        	Group 5                     *
; ***********************************************

.include 	"m32adef.inc" 						;


; Vector table setup
.org		0x000								; Sætter op vector table.  
			RJMP 	reset						; Aktivere PB1 til Målstregsensor
.org 		0x002								;			
			RJMP 	Start_Line					;
.org 		0x00C								; Aktivere input capture mode.  
.org		0x01A								; Aktivere modtagelse interrupt.
			JMP 	USART_Loop_Int				; 


; Port, Kommunikation, interupt and ADC Setup
			


; ***********************************************
;		              Setup                     *
; ***********************************************	

reset: 		
			LDI 	R16, 	HIGH(RAMEND)		; Opsætning stack pointer. 
			OUT 	SPH, 	R16					;
			LDI 	R16, 	LOW(RAMEND)			;
			OUT 	SPL, 	R16					;

; Port Setup			

			LDI 	R16, 	0x00				; PortA som input
			OUT 	DDRA, 	R16					;
			SBI 	DDRA, 	6					; Sæt til output
			SBI 	DDRA, 	5					; Sæt til output
			LDI 	R16, 	0xFF				; 
			OUT 	PORTA, 	R16					; 
			
			LDI 	R16, 	0x00				; PortB som input. 
			OUT 	DDRB, 	R16					;
			SBI		DDRB,	0					;
			LDI 	R16, 	0xFF				;
			OUT 	PORTB, 	R16					;

			LDI 	R16, 	0xFF				; PortC som output. 
			OUT 	DDRC, 	R16					; 
			SBI 	DDRC, 	1					;
			OUT 	PORTC, 	R16					;
	

			LDI 	R16, 	0xFF				; PortD som output. 
			OUT 	DDRD, 	R16					;
			CBI 	DDRD, 	2					; 
			SBI 	DDRD, 	7					;
			OUT 	PORTD, 	R16					;

; Kommunikation Setup


			LDI 	R16, 	0x02	 			; Overførelse hastighed til double. 
			OUT 	UCSRA, 	R16					; 

			LDI 	R16, 	0x98				; Aktivere modtagelse, afsendelse og interrupt.
			OUT 	UCSRB, 	R16					; Sætter data størrelse til 8 bit.  
			

			LDI 	R16, 	0x86				; Sættes til asynkron mode og ingen paritet med 1 stop bil.  
			OUT 	UCSRC, 	R16					; 

			LDI 	R16, 	0x00				; Sætter 9600 BPS som baud rate. 
			OUT 	UBRRH, 	R16					; 

			LDI 	R16, 	0xCF				; Sætter 9600 BPS som baud rate
			OUT 	UBRRL, 	R16					; 


; Interupt/Timer Setup
			  	
	
			LDI 	R16, 	0x40				; Aktivere interrup 1. 
			OUT 	GICR, 	R16					; Start line interrupt

			LDI 	R16, 	0x02				; 
			OUT 	MCUCR, 	R16					;
			
			LDI		R16, 	0x69				; Sætter op timer 2 
			OUT		TCCR2, 	R16					; til PWM Signal
			

; ADC Setup

			LDI		R16, 	0x80 				; Sætter ADC op.  
			OUT		ADCSRA, R16					; 
			LDI		R16, 	0x60				; 
			OUT		ADMUX, 	R16					; 


; ***********************************************
;		  Bluetooth Kommunikation mode          *
; ***********************************************

; Main 	Loop
			
			LDI 	R23, 	0x00				; Klargør register til RPM, høje ende. 
			LDI 	R24,	0x00				; Klargør register til RPM, lave ende.
			LDI 	R26, 	0x00				; Klargør register til bluetooth counting, hente alle 3 byte.  
			LDI 	R27, 	0x00				; Klargør register til omgange. 
			LDI 	R28, 	0x00				; Klargør register til at aktive mapping og race mode. 
			LDI 	R29, 	0x00				; Klargør register til at tælle antal sving. 

; Fail Safe
			LDI		R25, 	0x00				; Sætter PWM til 00. Bilen holder stille.   
			OUT		OCR2, 	R25					; 





USART_Loop:	
		
			LDI 	R22, 	0x00				; Slukker LED
			OUT 	PORTC, 	R22					;
			OUT 	PORTB, 	R22					;
; BT kommunikations Loop
 		
			SBIC	UCSRA, 	RXC					; Venter på at der er modtaget data. 
			RJMP 	Read_Data					; 
			
			
; Aktivere fail safe
			

			LDI 	R17, 	0x20				; Checker om fail safe er aktiveret.  
			OUT 	PORTA, 	R17					; 

			IN 		R16, 	PINA				; 			

			CPSE	R16, 	R17					;
			RJMP 	USART_Loop					; 


			RJMP	NoBT						; 
			
			 



; Branches. 

Read_Data: 	
									
			
			IN 		R21, 	UDR					; Henter bluetooth data.  
			SBI 	UCSRA, 	RXC					; Klargør ny modtagelse af data.  

			INC 	R26							; Henter alle tre byte. 
			
			CPI 	R26, 	0x01				; Henter de representative byte.  
			BREQ 	Type						; Henter type ellers gå videre til næste byte.  

			CPI 	R26, 	0x02				; Henter de representative byte. . 
			BREQ	Command						; Henter command ellers gå videre til næste byte.  

			CPI 	R26, 	0x03				; Henter de representatice byte.  
			BREQ 	Data						; Henter data ellers jump op og vent på ny data. . 

			LDI 	R26, 	0x00				; Genstarter byte count. 
			RJMP 	USART_Loop					; 




Type: 		
			SBIS	UCSRA, 	RXC					; Venter på modtagelse af næste byte. 
			RJMP 	Type						; 
			
			OUT 	UDR, 	R21					; Laver echo. 
			
			CPI 	R21, 	0x55				; Tjekker om det er gyldig data. 	
			BREQ 	Read_Data					; Hvis gyldig hop op og hent næste byte. 
						
			RJMP 	USART_Loop					; Ellers hent næste byte. 

Command: 	
			
			SBIS	UCSRA, 	RXC					; Venter på modtagelse af næste byte. 
			RJMP 	Command						; 

			OUT 	UDR, 	R21					; Laver echo. 
			
			CPI 	R21, 	0x10				; Tjekker om det er start eller stop. 
			BREQ 	Read_Data					; Hvis start hop op og hent næste byte. 
			
			CPI 	R21, 	0x11				; 
			BREQ	STOP						; Hvis stop hop ned til stop. 
			
			RJMP 	USART_Loop					; Ellers vent på modtagelse af ny data. 

Data: 		
			OUT 	UDR, 	R21					; Laver Echo
			OUT 	OCR2, 	R21					; Sætter ønsket PWM signal. 
			
			JMP 	NoBT						; Hop til NoBT
		
STOP: 	
			
			LDI 	R21, 	0x00				; Stopper bilen
			OUT 	OCR2, 	R21					;
			
			SBIC	UCSRA, 	RXC					; Holder stille indtil nye informationer er modtaget. 
			RJMP 	Read_Data					;
			
			RJMP 	Stop						; Venter på nye informationer. 




; ***********************************************
;		      Målstregs  interrupt              *
; ***********************************************

Start_Line:	
			
			LDI 	R31, 	0x00				; Sætter Z pointer op. 
			LDI 	R30, 	0x60				; 
			
			LDI 	R22, 	0x01				; Tænder LED'er 
			OUT 	PORTB, 	R22					;
			LDI 	R22, 	0x02				;
			OUT 	PORTC, 	R22					;		
												
			INC 	R27							; Tjekker om det er mapping eller race mode. 
			CPI 	R27, 	0x01				; 
			BREQ 	No_BT						; Hopper til mapping mode ellers hop til race mode. 
			
			INC 	R28							; Omgangs tæller.  
			LDI 	R27, 	0x01				; Holder sig til en test runde. 
			
			RJMP 	Begin_Race					; Hop til race mode.  

; ***********************************************
;		  Vnter på målstregs interrupt.         *
; ***********************************************			



NoBT: 			
			SEI									; Muligøre interrupt. 

			LDI		R25, 	0x49				; Sætter mapping speed.  
			OUT		OCR2, 	R25					; 
			
			RJMP 	NoBT						; Venter på Målstregs interrupt. 


; ***********************************************
; 					Mapping Mode			    *
; ***********************************************



No_BT:		
			LDI 	R16,	0x06				; Aktiver RPM tæller. 
			OUT 	TCCR1B, R16					; 
			
			LDI 	R25, 	0x4F				;
			OUT 	OCR2, 	R25					;


Mapping:	

READ_ADC:
			SBI		ADCSRA, ADSC				; Start ADC

KEEP_POLING:
			SBIC	ADCSRA, ADSC				; Venter på ADC er færdig. 
			RJMP 	KEEP_POLING					; 
			SBI		ADCSRA, ADIF				; Klargør til næste ADC. 
			
			SEI									; Muligøre interrupt. 

			LDI		R22, 	0x00				; Slukker LED. 
			OUT		PORTC, 	R22					;
			OUT 	PORTB, 	R22					;
			
			IN		R21, 	ADCH				; Henter ADCH, sving?
; Have parameter for kraftmåler på plads, 								
; ***********************************************	
			CPI		R21, 	90 ;82					; Sammenligner med værdi for sving. 
			BRSH	HIGHS						; Hvis højere, er det et venstre sving. 
; ***********************************************			
			RJMP	LOWS						; Hvis lavere er det et højere sving. 
; **********************************************	
LOWS:		
			CPI		R21, 	80 ;62					; Tjekker om det er et støj signal eller et sving. 
; ***********************************************
			BRSH	EQUAL						; Hvis det er støj, hop til equal. 
			RJMP	LOWLED      				; Ellers er det et højre sving. 
; ***********************************************
HIGHS:		
			CPI		R21, 	98 ;102					; Tjekker om det er et støj signal eller et sving. 
; ***********************************************
			BRSH	HIGHLED						; Hvis det er et venstre sving, hop dertil. 
			RJMP	EQUAL						; Ellers hop til Equal. 

LOWLED: 	
			
			LDI 	R22, 	0xFF				; Tænder LED. 
			OUT 	PORTB, 	R22					;

			INC 	R29							; Tæller sving. 

			IN 		R16, 	TCNT1L				; Henter RPM. 
			IN 		R17, 	TCNT1H				;

			LDI 	R25, 	0x00				; Sætter RPM til nul. 
			OUT 	TCNT1L, R25 				; Klargør til at tælle hvor langt svinget er. 
			OUT 	TCNT1H, R25					;
			
Read_ADCLow:
			SBI		ADCSRA, ADSC				; Klargør næste ADC. 

KEEP_POLINGLow:
			SBIC	ADCSRA, ADSC				; Venter på at konvertering er færdig. 
			RJMP 	KEEP_POLINGLow				; 
			SBI		ADCSRA, ADIF				; Klargør næste ADC input. 
; ***********************************************			
			IN 		R21, 	ADCH				; Henter værdi for sving.  
			CPI 	R21, 	88					; Tjekker om det er lige ud eller et sving. 
			BRLO	Read_ADCLow 				; Hvis det er et sving hop op og tjek igen, venter på at svinget er overstået. 	
; ***********************************************
			IN 		R19,	TCNT1L				; Henter længde på sving. 
			IN		R20, 	TCNT1H				;

			LDI 	R25, 	0x00				; Sætter RPM til nul. 
			OUT 	TCNT1L, R25					; Klargør til afstand til næste sving. 
			OUT 	TCNT1H, R25					;

			ADD 	R24, 	R16					; Tæller samlede antal RPM. 
			ADC 	R23, 	R17					;
			ADD 	R24, 	R19					; . 
			ADC 	R23, 	R20					;

			ST		Z+, 	R16					; Gemmer afstand til sving, svinget og længden på svinget.  
			ST		Z+, 	R17					; 
			ST		Z+, 	R18					; 
			ST 		Z+, 	R19					; 
			ST		Z+, 	R20					; 
			RJMP	READ_ADC					; 

EQUAL:		
			LDI		R22, 	0x00				; Slukker LED
			OUT 	PORTB, 	R22					;
			OUT		PORTC,	R22
			RJMP	READ_ADC					;

HIGHLED:	
			LDI 	R22, 	0xFF				; Tænder pågældende LED. 
			OUT 	PORTC, 	R22					;
			
			INC 	R29

			IN 		R16, 	TCNT1L				; Henter RPM op til dette sving. 
			IN 		R17, 	TCNT1H				;
			
			LDI 	R25, 	0x00				; Sætter RPM til 0. 
			OUT 	TCNT1L, R25					; Klargør at hente sving. 
			OUT 	TCNT1H, R25					;
			
Read_ADCHigh:
			SBI		ADCSRA, ADSC				; Start ADC

KEEP_POLINGHigh:
			SBIC	ADCSRA, ADSC				; Tjekker om ADC er færdig. 
			RJMP 	KEEP_POLINGHigh				; 
			SBI		ADCSRA, ADIF				; 
; ***********************************************			
			IN 		R21, 	ADCH				; Henter ADC.  
			CPI 	R21, 	65					; Tjekker om det er et sving eller lige ud. 
			BRSH	Read_ADCHigh				; Hvis et sving, bliv ved med at tjekke og vente på at svinget er overstået.  
; ***********************************************
			IN 		R16,	TCNT1L				; Henter længde på sving.  
			IN		R17, 	TCNT1H				;

			LDI 	R25, 	0x00				; Sætter RPM til 0
			OUT 	TCNT1L, R25					; Klargør til at registrere afstand til næste sving. 
			OUT 	TCNT1H, R25					;

			ADD 	R24, 	R16					; Gemmer nuværeden RPM. 
			ADC 	R23, 	R17					;
			ADD 	R24, 	R19					;  
			ADC 	R23, 	R20					;

			ST		Z+, 	R16					; Gemmer Længde til sving, svinger og længden på svinget. 
			ST		Z+, 	R17					; 
			ST		Z+, 	R18					; 
			ST 		Z+, 	R19					; 
			ST		Z+, 	R20					;
			RJMP	READ_ADC					;  



; ***********************************************
; 			      	 Race Mode	                *
; ***********************************************
			
Begin_Race:										 
			LDI 	R31, 	0x00				; Sætter Z pointer. 
			LDI 	R30, 	0x60				;
			
Race: 	
			SEI									; Muliggøre interrupt. 
			
			LDI 	R25, 	0x00				; Slukker LED. 
			OUT 	PORTC, 	R25					;
			OUT 	PORTB, 	R25					;
			
			LDI 	R25, 	0x00				; Sætter PWM til nul. 
			OUT 	TCNT1H, R25					; Klargør sammenligning mellem afstand og nuvrende RPM. 
			OUT 	TCNT1L, R25					;

			LDI 	R25, 	0x45				; Sætter hastighed på bilen.  
			OUT 	OCR2, 	R25					;

			LD		R21, 	Z+					; Tjekker hvor langt der er til næste sving.  			
 			LD 		R22, 	Z+					;
; Skal have testet hvor langt før et sving bilen skal sænke farten.
; Mit bud er 512 spoleviklinger før tid. 
; ***********************************************
			SUBI	R22, 	0x02				; Bremser op lidt før sving. 
; ***********************************************

Race1: 		
			IN 		R16, 	TCNT1L				; Henter nuværende RPM. 
			IN 		R17, 	TCNT1H				;
; ***********************************************
; DENNE LØSNING DUR IKKE. SKAL FINDDES EN ANDEN. 
; Skal have lagt de spoleviklinger til som er trukket fra tidligere. 
; Husk og holde øje med register for spoleviklingerne, er korrekte, High - High og Low - Low.  
; ***********************************************
			LDI 	R25, 	40					; Lægger bremse afstand til længde på svinget. 
; ***********************************************
			ADD 	R16, 	R25					;
; ***********************************************
			
			CP	 	R16, 	R21					; Tjekker hvor langt der er til næste sving. 
			BREQ 	Test						; 

			RJMP 	Race1						; Venter på næste sving.  

test: 		
			CP 		R17, 	R22					; Tjekker hvor langt der er til næste sving. 
			BRNE  	Race1						; 

 			LDI 	R25, 	0x00				; Sætter RPM til nul. 
			OUT 	TCNT1H, R25					; Klargør til sammenligning med længden af svinget. 
			OUT 	TCNT1L, R25					;

			LD 		R18, 	Z+					; Henter Sving og længde på sving. 
			LD 		R19, 	Z+					; 
			LD 		R20, 	Z					; 

			

; DENNE LØSNING DUR IKKE. SKAL FINDDES EN ANDEN. 
; Skal have lagt de spoleviklinger til som er trukket fra tidligere. 
; Husk og holde øje med register for spoleviklingerne, er korrekte, High - High og Low - Low.  
; ***********************************************
			LDI 	R25, 	40					; Lægger bremse afstand til længde på svinget. 
; ***********************************************
			ADD 	R20, 	R25					;
; Er det muligt at placere den her???? 		

			MOV 	R21,	R19					; Gemmer nuværende position.  
			MOV		R22, 	R20					; 
			
			LSR 	R22							; Tjekker om bilen er halvejs i svinget.  
			LSR 	R21							; 

turn:
			IN 		R19, 	TCNT1L				; Henter RPM
			IN 		R20, 	TCNT1H				;			
	
			CP		R19, 	R21					; Tjekker hvor langt i svinget bilen er. 
			BRNE 	turn						; 

			CP 		R20, 	R22					; samme som før. 
			BRNE 	turn						;
; Skal have testet hastigheder rundt på banen. 
; ***********************************************			
			LDI 	R25, 	0x20				; Sender tilpas PWM ud. 
			OUT 	OCR2, 	R25					;
; ***********************************************			
			MOV 	R19, 	R21					; Henter næste check point i svinget.  
			LSR 	R19							;
			ADD 	R21, 	R19					;

			MOV 	R20, 	R22					; 
			LSR 	R20							;
			ADD 	R22, 	R20					;

threequarter:

			IN 		R19, 	TCNT1L				; Henter nuværende position. 
			IN 		R20, 	TCNT1H				;

			CP		R19, 	R21					; Venter på at bilen er på ønskes position. 
			BRNE 	threequarter				; 

			CP 		R20, 	R22					;
			BRNE 	threequarter				;
; ***********************************************
			LDI 	R25, 	0x25				; Sætter ny PRM signal. 
			OUT 	OCR2, 	R25					;
; ***********************************************			

			LD		R22,	-Z					; Tilpasser Z pointer
			LD		R21,  	Z+					; Klargør til udgang af sving
			LD	 	R18, 	Z+					;

Exit_Turn:
			
			IN 		R19,	TCNT1L				; Tjekker hvor langt der er til næste sving.  
			IN 		R20, 	TCNT1H				;
			
			CP		R21, 	R19					; Tjekker om bilen er ude af sving. 
			BRNE 	Exit_Turn					;

			CP		R22, 	R20					; samme somm ovenstående.
			BRNE 	Exit_Turn					;

; Skal have undersøgt og testet hvor langt der skal være hen til næste sving. 
; ***********************************************
			LDD 	R22, 	Z+1					; Læser afstand til næste sving. 
			CPI		R22, 	0x02				; Tjekker afstand til næste sving. 
			BRSH 	Return						; 
; ***********************************************
			LDI 	R25,	0x50				; Sæt tilpas PWM. 
			OUT 	OCR2, 	R25					;
; ***********************************************			

Return: 	JMP 	Race						; Mellem hop til race. 

; ***********************************************
; 			RXC Interrupt loop					*
; ***********************************************


USART_Loop_Int:	
			LDI 	R26, 	0x00				; Klargør ny tælling til BT. 

			SBIC	UCSRA, 	RXC					; Venter på modtagelse af data. 
			RJMP 	Read_Data_Int				; 

			RETI	

Read_Data_Int_1:
			OUT 	UDR, 	R21					; Sender data tilbage til program. 

Read_Data_Int: 							
			
			IN 		R21, 	UDR					; Henter information.  
			SBI 	UCSRA, 	RXC					; Klargør til næste modtagelse. 

			INC 	R26							; Henter alle 3 byte.  
			
			CPI 	R26, 	0x01				; Henter første byte.  
			BREQ 	Type_Int				    ; 

			CPI 	R26, 	0x02				; Henter anden byte. 
			BREQ	Command_Int					; 
			
			CPI 	R26, 	0x03				; Henter tredje byte.  
			BREQ 	Data_Int					; 
			
			RJMP 	USART_Loop_Int				; 




Type_Int: 		
			SBIS	UCSRA, 	RXC					; Venter på at type er modtaget. 
			RJMP 	Type_Int					; 
			
			CPI 	R21, 	0xAA				; Tjekker typen. 
			BREQ 	Send_Data_Int				; Hopper til pågældende kommando. 
			
			CPI 	R21, 	0x55				; 
			BREQ	read_data_int_1				;
		
			RJMP 	USART_Loop_Int				; Hvis ikke gyldig hop tilbage. 

Command_Int: 	
			
			SBIS	UCSRA, 	RXC					; Venter på næste data er modtaget. 
			RJMP 	Command_Int					; 

			OUT 	UDR, 	R21					; Laver echo. 
			
			CPI 	R21, 	0x10				; Tjekker om det er start eller stop. 
			BREQ 	Read_Data_Int				; Hopper til pågældende data. 
			
			CPI 	R21, 	0x11				; 
			BREQ	STOP_Int					; 
			
			RJMP 	USART_Loop_Int				;
			 
Data_Int: 		
			OUT 	UDR, 	R21					; Sender data og sætter PWM signal. 
			OUT 	OCR2, 	R21					; 

			JMP 	USART_Loop_Int				; 

STOP_Int: 	
			LDI 	R25, 	0x00				; Stopper bilen og venter på ny data er modtaget. 
			OUT 	OCR2, 	R25					;
			SBIC	UCSRA, 	RXC					; 
			RJMP 	Read_Data_Int				;

			RJMP 	Stop_Int					; 


Send_Data_Int:

			IN 		R21, 	UDR					; Henter næste data. 
			SBI 	UCSRA, 	RXC					; Klargør modtagekse at næste informationer. 

			INC 	R26							; Tæller antal byte.  
			
			CPI 	R26,	0x02				; Tjekker data og hopper til det nødvendige. 
			BREQ	Command_Send				;
			
			CPI 	R26, 	0x03				; 
			BREQ 	Data_Send					; 

			RJMP 	USART_Loop_Int				; 


Command_Send: 
			SBIS	UCSRA, 	RXC					; Tekker om bilen er klar til at modtage næste data. 
			RJMP 	Command_Send				; 

			CPI 	R21, 	0x12				; Tjekker hvilken forspørgelse af data. 
			BREQ 	Send_RPM					; Hopper til det nødvendige. 
		
			CPI 	R21, 	0x13				;
			BREQ	Send_Sving					;
		
			CPI 	R21, 	0x14				;
			BREQ	Send_Start_Line				;			
			
			RJMP 	USART_Loop_Int
Data_Send: 	
			LDI 	R21, 	0x00				; Sender ingen ting. 
			OUT 	UDR, 	R21					;
			RJMP 	USART_Loop_Int				; 


Send_RPM:
			LDI 	R22, 	0xBB				; Sender reply command. 
		
			OUT 	UDR, 	R22					; 
			
reply4:		SBIS	UCSRA, 	TXC					; Checker om bilen er klar til at afsende nye data. 
			RJMP 	reply4						; 

			
			OUT 	UDR, 	R21					; Sender reply på forspørgelse af kommando.  

Command4:	SBIS	UCSRA, 	TXC					; Checker om bilen er klar til at afsende nye data. 
			RJMP 	Command4					; 

			OUT 	UDR, 	R23					; Sender RPM H

RPMH4:		SBIS	UCSRA, 	TXC					; Venter på at bilen er klar til at afsende nye data. 
			RJMP 	RPMH4						; 

			OUT 	UDR, 	R23					; Sender RPM L

RPMH5:		SBIS	UCSRA, 	TXC					; Venter på at bilen er klar til at afsende nye data. 
			RJMP 	RPMH5						; 

			RETI

Send_Sving:	
			LDI 	R22, 	0xBB				; Sender reply

			OUT		UDR, 	R22

Reply5:		SBIS	UCSRA, 	TXC					; Venter på at bilen kan sende ny informationer. 
			RJMP 	Reply5						; 
			
			OUT 	UDR, 	R21					; Sender Reply på kommando 

Command5:	SBIS	UCSRA, 	TXC					; Venter på at bilen kan sende ny informationer. 
			RJMP 	Command5					; 

			OUT 	UDR, 	R29					; Sender efterspurgt data. 

Sving5:		SBIS	UCSRA, 	TXC					; Venter på at bilen kan sende ny informationer. 
			RJMP 	Sving5						; 

			RETI

Send_Start_Line:

			LDI 	R22, 	0xBB				; Sender reply. 

			OUT 	UDR, 	R22					;

Reply6:		SBIS	UCSRA, 	TXC					; Venter på modtagelse af ny data. 
			RJMP 	Reply6						; 

			OUT 	UDR, 	R21					; Sender reply på kommando. 

Command6:	SBIS	UCSRA, 	TXC					; Venter på modtagelse af ny data. 
			RJMP 	Command6					; 


			OUT 	UDR, 	R28					; Sende efterspurgte data. 

Lap6:		SBIS	UCSRA, 	TXC					; Venter på modtagelse af ny data. 
			RJMP 	Lap6						; 
				
			RETI

